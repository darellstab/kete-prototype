import React from 'react';
import { TextField } from 'formik-material-ui';
import { Field } from 'formik';


export default function TextInput() {

    return (
        <div>
            <Field
                component={TextField}
                name="description"
                label="Beschreibung"
                required
                fullWidth
                rows={4}
                variant="filled"
                multiline
            />
        </div>
    );
}
