import { Avatar, Box, Card, Typography } from '@material-ui/core';
import React from 'react';
import {MatchType} from "../types/match";
import {useStyles} from "../App";

type MatchProps = {
    match: MatchType
}

export default function MatchBox({match} : MatchProps) {

    const classes = useStyles();

    return (
        <Card className={classes.root} >
            <Box justifyContent="center">
                <Avatar alt={match.firstName} src={match.imageUrl} className={classes.large} />
                <Typography gutterBottom variant="h5" component="h2">
                    {match.firstName}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    {match.distance}km entfernt
                </Typography>
                <Typography variant="body2" component="p">
                    Darf dir {match.firstName} helfen?
                </Typography>
            </Box>
        </Card>
    );
}
