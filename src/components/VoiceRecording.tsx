import React from 'react';
import AudioPlayer from 'material-ui-audio-player';
import {Button, Typography} from "@material-ui/core";

type VoiceRecordingProps = {
    url: string,
    name: string,
    onRemove: () => void
}

export default function VoiceRecording({url, name, onRemove} : VoiceRecordingProps) {


    return (
        <div>
            <Typography variant="h5">
                {name}
            </Typography>
            <Button onClick={onRemove}>
                entfernen
            </Button>
            <div>
                <AudioPlayer src={url} />
            </div>
        </div>
    );
}
