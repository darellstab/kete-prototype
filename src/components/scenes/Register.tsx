import React from 'react';
import {useStyles} from "../../App";
import {Button, LinearProgress, Typography} from "@material-ui/core";
import {Field, Form, Formik} from "formik";
import {TextField} from "formik-material-ui";
import {Link, useHistory} from "react-router-dom";
import { RegisterSchema } from '../../validation/registerSchema';

export default function Register() {

    const classes = useStyles();
    const history = useHistory();

    return (
        <div className={classes.root}>
            <Typography variant="h4">
                Registrierungsformular
            </Typography>

            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    street: '',
                    zip: '',
                    city: '',
                    email: ''
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        setSubmitting(false);
                        history.push("/registered");
                    }, 800);
                }}
                validationSchema={RegisterSchema}
            >
                {({ submitForm, isSubmitting, errors , isValid, dirty}) => (
                    <Form className={classes.root}>

                        <Field
                            component={TextField}
                            name="firstName"
                            label="Vorname"
                            required
                        />
                        <Field
                            component={TextField}
                            name="lastName"
                            label="Nachname"
                            required
                        />
                        <Field
                            component={TextField}
                            name="street"
                            label="Strasse und Nr."
                            required
                            fullWidth
                        />
                        <Field
                            component={TextField}
                            name="zip"
                            label="PLZ"
                            required
                        />
                        <Field
                            component={TextField}
                            name="city"
                            label="Stadt / Ort"
                            required
                        />
                        <Field
                            component={TextField}
                            name="email"
                            label="E-Mail"
                            required
                            fullWidth
                        />

                        {isSubmitting && <LinearProgress />}

                        <div className={classes.root}>
                            <Button color="secondary" variant="contained" component={Link}  to="/">Abbrechen</Button>
                            <Button color="primary" variant="contained"  disabled={(isSubmitting || !dirty || !isValid)} onClick={submitForm} >Registrieren</Button>
                        </div>
                    </Form>)}
            </Formik>
        </div>
    );
}
