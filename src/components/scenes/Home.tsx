import React from 'react';
import {Typography} from "@material-ui/core";

export default function Home() {
  return (
    <div className="Home">
        <Typography variant="h4">
            KETE Prototyp
        </Typography>

        <Typography variant="body1">
            Das ist der Prototyp für das Projekt "Serviceplattform zur Unterstützung von Risikogruppen während der Covid-19 Pandemie" im Rahmen des MScWI-Moduls "Key-Technologies".
        </Typography>

        <Typography variant="body1">
            Umgesetzt mit React / TypeScript - Code einsehbar via <a href="https://bitbucket.org/darellstab/kete-prototype/src/master/" target="_blank" rel="noreferrer">Bitbucket-Repository</a>
        </Typography>
    </div>
  );
}
