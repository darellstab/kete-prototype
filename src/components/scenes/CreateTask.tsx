import {Button, ButtonGroup, Checkbox, Fab, FormControl, Input, InputLabel,
    LinearProgress,
    ListItemText, MenuItem, Select, Typography} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { TextField } from 'formik-material-ui';
import { DateTimePicker } from 'formik-material-ui-pickers';
import React, {useState} from 'react';
import {InputType} from "../../types/inputType";
import {useStyles} from "../../App";
import VoiceInput from "../VoiceInput";
import TextInput from '../TextInput';
import {Link, useHistory} from "react-router-dom";
import { Formik, Form, Field } from 'formik';
import {categories} from "../../data/categories";
import {CreateTaskSchema} from "../../validation/createTaskSchema";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import {VoiceRecordingType} from "../../types/voiceRecordingType";



export default function CreateTask() {

    const classes = useStyles();
    const history = useHistory();


    const [selectedInputType, setSelectedInputType] = useState<InputType>(InputType.Text);
    const [selectedCategory, setSelectedCategory] = useState<string[]>([]);
    const [recordings, setRecordings] = useState<VoiceRecordingType[]>([]);

    return (
        <div className={classes.root}>
            <Typography variant="h4">
                Neuen Auftrag erfassen
            </Typography>

            <Formik
                initialValues={{
                    title: '',
                    description: '',
                    voiceDescription: null,
                    date: null,
                    categories: ''
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        setSubmitting(false);
                        history.push("/success");
                    }, 500);
                }}
                validationSchema={CreateTaskSchema}
            >
                {({ submitForm, isSubmitting, errors , isValid, dirty}) => (
                    <Form className={classes.root}>

                        <Field
                            component={TextField}
                            name="title"
                            label="Titel des Auftrags"
                            required
                            fullWidth
                        />

                        <div>
                            <ButtonGroup disableElevation aria-label="outlined primary button group">
                                <Button color={selectedInputType === InputType.Text ? "primary" : "default"} onClick={() => setSelectedInputType(InputType.Text)}>Text</Button>
                                <Button color={selectedInputType === InputType.Voice ? "primary" : "default"} onClick={() => setSelectedInputType(InputType.Voice)}>Sprache</Button>
                            </ButtonGroup>
                        </div>

                        <div>
                            {selectedInputType === InputType.Text ? <TextInput /> : <VoiceInput voiceRecordings={recordings} setVoiceRecordings={setRecordings} /> }
                        </div>

                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Field
                                component={DateTimePicker}
                                label="Zeitpunkt des Auftrags"
                                name="date"
                                format="MM.dd.yyyy, HH:mm"
                            />
                        </MuiPickersUtilsProvider>


                        <div>
                            <FormControl fullWidth>
                                <InputLabel id="demo-mutiple-checkbox-label">Leistungskategorien</InputLabel>
                                <Select
                                    labelId="demo-mutiple-checkbox-label"
                                    id="demo-mutiple-checkbox"
                                    multiple
                                    input={<Input />}
                                    value={selectedCategory}
                                    onChange={(event) => setSelectedCategory(event.target.value as string[])}
                                    renderValue={(selected : any) => selected.join(', ')}
                                    fullWidth
                                >
                                    {categories.map((name : string) => (
                                        <MenuItem key={name} value={name}>
                                            <Checkbox checked={selectedCategory.indexOf(name) > -1} />
                                            <ListItemText primary={name} />
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </div>

                        <div>
                            <label htmlFor="upload-photo">
                                <input
                                    style={{ display: 'none' }}
                                    id="upload-photo"
                                    name="upload-photo"
                                    type="file"
                                />

                                <Fab
                                    color="primary"
                                    size="small"
                                    component="span"
                                    aria-label="add"
                                    variant="extended"
                                >
                                    <AddIcon /> Anhang hinzufügen
                                </Fab>
                            </label>
                        </div>

                        {isSubmitting && <LinearProgress />}

                        <div className={classes.root}>
                            <Button color="secondary" variant="contained" component={Link}  to="/">Abbrechen</Button>
                            <Button color="primary" variant="contained"  disabled={(isSubmitting || !dirty || !isValid)} onClick={submitForm} >Veröffentlichen</Button>
                        </div>
                    </Form>)}
            </Formik>
        </div>
    );
}
