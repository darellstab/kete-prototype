import React from 'react';
import MuiAlert from '@material-ui/lab/Alert';
import {Button, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useStyles} from "../../App";

export default function Success() {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Typography variant="h4">
                Auftrag erfasst
            </Typography>

            <br/>

            <MuiAlert elevation={6} variant="filled" severity="success">Der Auftrag wurde erfolgreich
                erfasst.</MuiAlert>

            <br/>

            <div>
                <Button color="secondary" variant="contained" component={Link} to="/">Zurück zur Startseite</Button>
            </div>
        </div>
    );
}
