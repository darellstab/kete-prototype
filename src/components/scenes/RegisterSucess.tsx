import React from 'react';
import MuiAlert from '@material-ui/lab/Alert';
import {Button, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useStyles} from "../../App";

export default function RegisterSuccess() {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Typography variant="h4">
                Registrierung erfolgreich
            </Typography>

            <br/>

            <MuiAlert elevation={6} variant="filled" severity="success">Sie haben sich erfolgreich registriert</MuiAlert>

            <br/>

            <div>
                <Button color="secondary" variant="contained" component={Link} to="/">Zurück zur Startseite</Button>
            </div>
        </div>
    );
}
