import React, {useEffect, useState} from 'react';
import {Button, LinearProgress, Typography} from "@material-ui/core";
import {useStyles} from "../../App";
import {MatchType} from "../../types/match";
import {staticMatches} from "../../data/matches";
import MatchBox from "../MatchBox";
import {useHistory} from "react-router-dom";

export default function Match() {

    const classes = useStyles();
    const history = useHistory();

    const [matches,setMatches] = useState<MatchType[]>([]);
    const [currentMatch, setCurrentMatch] = useState<number>(0);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [success, setSuccess] = useState<boolean>(false);

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
            setMatches(staticMatches);
        }, 900)
    }, []);

    const showMatch = matches[currentMatch];

    const nextMatch = () => {
        setIsLoading(true);

        setTimeout(() => {
            setIsLoading(false);
            setCurrentMatch(currentMatch+1);
        }, 1100);
    };

    const acceptMatch = () => {
        setIsLoading(true);
        setSuccess(true);

        setTimeout(() => {
            setIsLoading(false);
            history.push("/matched");
        }, 800);
    };

    return (
        <div className={classes.root}>
            <Typography variant="h4">
                Match
            </Typography>

            {showMatch === undefined ?  (!isLoading && <Typography component="span" variant="body2">Keine weiteren Matches gefunden, bitte schauen Sie später wieder vorbei.</Typography>) :
                <MatchBox match={showMatch} />
            }

            {isLoading &&
                    <>
						<LinearProgress />
                        {!success && <Typography variant="body2">
							Finde passende Matches...
                        </Typography>}
                    </>
            }

            { showMatch &&
                <div className={classes.root}>
                    <Button color="secondary" variant="contained" onClick={nextMatch}>Nein</Button>
                    <Button color="primary" variant="contained" onClick={acceptMatch}>Ja</Button>
                </div>
            }
        </div>
    );
}
