import React, {useState} from 'react';
import {ReactMic, ReactMicStopEvent} from 'react-mic';
import {IconButton} from "@material-ui/core";
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import VoiceRecording from "./VoiceRecording";
import {VoiceRecordingType} from "../types/voiceRecordingType";
import { v4 as uuidv4 } from 'uuid';

type VoiceInputProps = {
    voiceRecordings: VoiceRecordingType[],
    setVoiceRecordings: (newVoiceRecord: VoiceRecordingType[]) => void
}

export default function VoiceInput({voiceRecordings, setVoiceRecordings} : VoiceInputProps) {

    const [isRecording, setIsRecording] = useState<boolean>(false);

    const addRecording = (recordedData: ReactMicStopEvent) => {
        const newRecording = {
            uid: uuidv4(),
            name: 'Aufnahme ' + recordedData.startTime,
            url: recordedData.blobURL
        };

        const recordings = [...voiceRecordings, newRecording];

        setVoiceRecordings(recordings);
    };

    const removeRecording = (uid: string) => {
        const recordings = voiceRecordings.filter(item => item.uid !== uid);

        setVoiceRecordings(recordings);
    };

    console.log(voiceRecordings);

    return (
        <div>
            <ReactMic
                record={isRecording}
                className="sound-wave"
                onStop={addRecording}
                strokeColor="#000000"
                backgroundColor="#F0F0F0"/>

            <IconButton onMouseDown={() => setIsRecording(true)} onMouseUp={() => setIsRecording(false)}>
                <FiberManualRecordIcon fontSize="large" color={isRecording ? 'secondary' : 'primary'}/>
            </IconButton>

            <div>
                {voiceRecordings.length > 0 && voiceRecordings.map((voiceRecording) => <VoiceRecording key={voiceRecording.uid}
                                                                                             url={voiceRecording.url}
                                                                                             name={voiceRecording.name}
                                                                                             onRemove={() => removeRecording(voiceRecording.uid)}/>)}
            </div>

        </div>
    );
}
