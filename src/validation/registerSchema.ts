import * as Yup from 'yup';

export const RegisterSchema = Yup.object().shape({
    firstName: Yup.string()
        .required('Pflichtfeld'),
    lastName: Yup.string()
        .required('Pflichtfeld'),
    street: Yup.string()
        .required('Pflichtfeld'),
    zip: Yup.number()
        .required('Pflichtfeld'),
    city: Yup.string()
        .required('Pflichtfeld'),
    email: Yup.string()
        .email('Muss eine gültige E-Mail Adresse sein')
        .required('Pflichtfeld')

});
