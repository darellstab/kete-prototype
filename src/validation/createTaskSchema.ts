import * as Yup from 'yup';

export const CreateTaskSchema = Yup.object().shape({
    title: Yup.string()
        .required('Pflichtfeld'),
    date: Yup.date()
        .required('Pflichtfeld'),
    description: Yup.string()
        .required('Pflichtfeld')
});
