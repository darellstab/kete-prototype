

export type MatchType = {
    uid: number
    firstName: string,
    distance: number,
    imageUrl: string
}
