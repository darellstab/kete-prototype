

export type VoiceRecordingType = {
    uid: string
    url: string,
    name: string
}
