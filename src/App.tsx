import {Button, Container} from '@material-ui/core';
import React from 'react';
import './App.css';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Home from './components/scenes/Home';
import CreateTask from "./components/scenes/CreateTask";
import Register from './components/scenes/Register';
import Match from "./components/scenes/Match";
import {makeStyles} from "@material-ui/core/styles";
import Success from "./components/scenes/Success";
import RegisterSuccess from "./components/scenes/RegisterSucess";
import MatchSuccess from "./components/scenes/MatchSuccess";


export const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
}));

function App() {

    return (
        <div>
            <Router>
                <Container maxWidth="sm">
                    <nav>
                        <Button variant="contained" component={Link} to="/">Home</Button>
                        <Button variant="contained" component={Link} to="/create">Auftrag erfassen</Button>
                        <Button variant="contained" component={Link} to="/register">Registrieren</Button>
                        <Button variant="contained" component={Link} to="/match">Matches</Button>
                    </nav>

                    <Switch>
                        <Route path="/create">
                            <CreateTask/>
                        </Route>
                        <Route path="/register">
                            <Register/>
                        </Route>
                        <Route path="/match">
                            <Match/>
                        </Route>
                        <Route path="/success">
                            <Success/>
                        </Route>
                        <Route path="/registered">
                            <RegisterSuccess/>
                        </Route>
                        <Route path="/matched">
                            <MatchSuccess/>
                        </Route>
                        <Route path="/">
                            <Home/>
                        </Route>
                    </Switch>
                </Container>
            </Router>
        </div>
    );
}

export default App;
