import {MatchType} from "../types/match";

export const staticMatches : MatchType[] = [
    {
        uid: 1,
        firstName: 'Samuel',
        distance: 23,
        imageUrl: 'https://via.placeholder.com/150'
    },
    {
        uid: 2,
        firstName: 'Kathrin',
        distance: 12,
        imageUrl: 'https://via.placeholder.com/150'
    },
    {
        uid: 3,
        firstName: 'Theo',
        distance: 35,
        imageUrl: 'https://via.placeholder.com/150'
    },
];
